require('dotenv').config()
const express = require('express');
const mongoose = require('mongoose')
const app = express();

app.use(
    express.urlencoded({
        extended: true
    })
)

app.use(express.json())

const personRoutes = require('./routes/personRoutes')
const gastoRoutes = require('./routes/gastoRoutes')

app.use('/person', personRoutes)
app.use('/gasto', gastoRoutes)

app.get('/', (req,res)=>{
    res.json({message: 'oi Express'})
})
const DB_USER = process.env.DB_USER
const DB_PASSWORD = encodeURIComponent(process.env.DB_PASSWORD)

mongoose.connect(`mongodb+srv://${DB_USER}:${DB_PASSWORD}@zeuscluster.tibv4g1.mongodb.net/projectzeusdb?retryWrites=true&w=majority`).then(
    ()=>{
        console.log("Conectado ao MongoDB")
        app.listen(3000)
    }
).catch((err)=>console.log(err)

)

//app.listen(3000)

